#ifndef _CALC_H_
#define _CALC_H_

/** Função que recebe uma expressão infix e calcula seu resultado
  * @param expr string terminada por \0 contendo a expressão
  * @param err código de erro
  * @return valor da expressão
  */ 
int calculadora(char * expr, int * err);


#endif /* _CALC_H_ */